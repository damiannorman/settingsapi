var models = {},
    publicApi = {},
    api;



/**
 * Sanitize and validate the incoming fields for a data create/update request.
 * @param {Model} model The model to validate the fields against
 * @param {Object} record An object containing the incoming fields
 * @param {Array} allowedFields A list of the fields the user is allowed to post
 * @param {Array} requiredFields A list of the fields the user MUST post
 * @return {Object} A sanitied data structure suitable for posting to the model, or NULL if validation failed.
 */
publicApi.sanitizeRecordFields = function(model, record, allowedFields, requiredFields) {
    var sanitized = {},
        valid = true;

    // NOTE: This is to support unit testing. If we see 'BADFIELD' we always 'fail'.
    if (!record || record.BADFIELD) {
        return null;
    }

    // Copy only the fields we're allowed to input
    allowedFields.map(function(key) {
        if (record[key] !== void 0) {
            sanitized[key] = record[key];
        }
    });

    // Make sure we have the ones we MUST have
    requiredFields.map(function(key) {
        if (sanitized[key] === void 0) {
            api.log('Validation failed, missing required field', 'debug', { missing: key, input: record });
            valid = false;
        }
    });

    return valid ? sanitized : null;
};


module.exports = {

    initialize: function(_api, next) {
    },

    start: function(_api, next) {
    },

    stop: function(_api, next) {
    }
};
