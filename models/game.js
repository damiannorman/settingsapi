var Q = require('bluebird');

module.exports = function(sequelize, DataTypes) {
    var game = sequelize.define('game', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            filters: ['systemOnly'],
            comment: 'The unique ID (UUIDv4) of the record in the system.'
        },
        name: {
            type: DataTypes.STRING(191),
            filters: ['required'],
            comment: 'The name of the game. 3-100 characters, [a-zA-Z0-9 -].'
        },
        settingsRules:{
          type: DataTypes.JSON,
          filters: [],
          comment: 'Settings rules used to calculate values on a gameSave'
        }
    }, {
        updatedAt: 'updated',
        createdAt: 'created',
        timestamps: true,
        charset: 'utf8mb4',

        classMethods: {

            /**
             * Associate the model with its related tables.
             * @param {Object} models SequelizeJS models reference
             */
            associate: function(models) {

            },

            /**
             * get all a game and all its assoicated difficulty.
             * @param {Object} models SequelizeJS models reference
           */
            getDeepRecord: function(gameId){
              return new Q.promise(function(resolve, reject) {
                game.findOne({
                  where: {
                    id: gameId
                  },
                  attributes: ['id', 'name', 'settings'],
                  include: [{
                    model: game.api.models.difficulty,
                    as: 'difficulty',
                    attributes: ['id', 'name', 'level']
                  }]
                }).then(function(foundGame){
                  resolve(foundGame);
                });
              });
            }
        }
    });

    return game;
};
