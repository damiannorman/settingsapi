var Q = require('bluebird');

module.exports = function(sequelize, DataTypes) {
    var diffculty = sequelize.define('diffculty', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            filters: ['systemOnly'],
            comment: 'The unique ID (UUIDv4) of the record in the system.'
        },
        name: {
            type: DataTypes.STRING(191),
            filters: [],
            comment: 'The name of the difficult. Must be unique. 3-100 characters, [a-zA-Z0-9 -].'
        },
        level: {
            type: DataTypes.INTEGER,
            filters: [],
            comment: 'Applied to a games\'s settings to calculate values'
        },
        diffcultyCategoryId: {
            type: DataTypes.UUID,
            filters: ['systemOnly'],
            comment: 'The unique ID (UUIDv4) of the associated diffcultyCategory'
        }
    }, {
        updatedAt: 'updated',
        createdAt: 'created',
        timestamps: true,
        charset: 'utf8mb4',
        classMethods: {
            /**
             * Associate the model with its related tables.
             * @param {Object} models SequelizeJS models reference
             */
            associate: function(models) {
              diffculty.hasOne(diffculty.api.models.game);
            },

          /**
           * Take the difficulty.level and normalize to the correct difficulty category
           * and then associate the difficulty and difficulty category
           * @param targetDiffculty
           * @returns {promise}
           */
            applyCorrectDifficultyCategory: function(targetDiffculty) {

              var categoryId, categories, difficulties;

              return new Q.promise(function (resolve, reject) {

                diffculty.api.models.diffcultyCategory.findAll().then(function(foundCategories){

                  categories = foundCategories;
                  return diffculty.api.models.diffculty.findAll();

                }).then(function(foundDiffculties){

                  difficulties = foundDiffculties.length;
                  categoryId = Math.round(targetDiffculty.level / difficulties) * categories;

                  return diffculty.api.models.diffcultyCategory.findOne(categoryId);

                }).then(function(category){

                  targetDiffculty.diffcultyCategoryId = category.id;
                  resolve(targetDiffculty);

                }).catch(function(e){
                  reject(e);
                });
              });
            },

          /**
           * Gets the difficulty by Id and associated difficulty category
           * @param diffcultyId
           * @returns {Q.promise}
           */
            getDeepRecord: function(diffcultyId){
              return new Q.promise(function(resolve, reject) {
                diffculty.findOne({
                  where: {
                    id: diffcultyId
                  },
                  attributes: ['id', 'name', 'level'],
                  include: [{
                    model: game.api.models.diffcultyCategory,
                    as: 'diffcultyCategory',
                    attributes: ['id', 'category']
                  }]
                }).then(function(foundDifficulty){
                  resolve(foundDifficulty);
                }).catch(function(e){
                  reject(e);
                });
              });
            },

          /**
           * Gets matching difficulties by level and associated difficulty categories
           * @param diffcultyId
           * @returns {Q.promise}
           */
            getDeepRecords: function(level, category){
              return new Q.promise(function(resolve, reject) {
                diffculty.findAll({
                  where: {
                    level: level,
                    category: category
                  },
                  attributes: ['id', 'name', 'level'],
                  include: [{
                    model: game.api.models.diffcultyCategory,
                    as: 'diffcultyCategory',
                    attributes: ['id', 'category']
                  }]
                }).then(function(foundDifficulty){
                  resolve(foundDifficulty);
                }).catch(function(e){
                  reject(e);
                });
            });
          }
        }
    });

    return diffculty;
};
