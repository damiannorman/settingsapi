module.exports = function(sequelize, DataTypes) {
    var diffcultyCategory = sequelize.define('diffcultyCategory', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            filters: ['systemOnly'],
            comment: 'The unique ID (UUIDv4) of the record in the system.'
        },
        category: {
            type: DataTypes.STRING(191),
            filters: [],
            comment: 'The name of the category..ie easy, medium or hard.'
        }
    }, {
        updatedAt: 'updated',
        createdAt: 'created',
        timestamps: true,
        charset: 'utf8mb4',
        classMethods: {
            /**
             * Associate the model with its related tables.
             * @param {Object} models SequelizeJS models reference
             */
            associate: function(models) {
              diffcultyCategory.hasOne(diffcultyCategory.api.models.diffculty);
            }
        }
    });

    return diffcultyCategory;
};
