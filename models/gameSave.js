var Q = require('bluebird');

module.exports = function(sequelize, DataTypes) {
    var gameSave = sequelize.define('gameSave', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
            filters: ['systemOnly'],
            comment: 'The unique ID (UUIDv4) of the record in the system.'
        },
        difficultyId: {
            type: DataTypes.UUID,
            filters: ['systemOnly'],
            comment: 'The unique ID (UUIDv4) of the save\'s difficulty, which can provide the level to determine settings.'
        },
        gameId:{
          type: DataTypes.UUID,
          filters: ['systemOnly'],
          comment: 'The unique ID (UUIDv4) of the game for which this is a save'
        },
        settings:{
          type: DataTypes.JSON,
          filters: [],
          comment: 'Settings key values'
        },
        stats:{
          type: DataTypes.JSON,
          filters: [],
          comment: 'Actual player stats, using settings'
        }
    }, {
        updatedAt: 'updated',
        createdAt: 'created',
        timestamps: true,
        charset: 'utf8mb4',

        classMethods: {

            /**
             * Associate the model with its related tables.
             * @param {Object} models SequelizeJS models reference
             */
            associate: function(models) {

            },

            getDeepRecord: function(gameSaveId){
              return new Q.promise(function(resolve, reject) {
                gameSave.findOne({
                  where: {
                    id: gameSaveId
                  },
                  attributes: ['id', 'settings'],
                  include: [{
                    model: gameSave.api.models.game,
                    as: 'game',
                    attributes: ['id', 'name', 'settings']
                  }]
                }).then(function(foundGame){
                  resolve(foundGame);
                });
              });
            },

            /**
            * get all gamesaves by optional difficulty level and difficulty category
            * @param level
            * @param category
            * @returns {Q.promise}
            */
            getDeepRecords: function(level, category) {
              return new Q.promise(function (resolve, reject) {
                gameSave.findAll({
                  where: {
                    level: level,
                    category: category
                  },
                  attributes: ['id', 'settings'],
                  include: [{
                    model: gameSave.api.models.diffculty,
                    as: 'diffculty',
                    attributes: ['id', 'name', 'level']
                  }, {
                    model: gameSave.api.models.diffcultyCategory,
                    as: 'diffcultyCategory',
                    attributes: ['id', 'category']
                  }]
                }).then(function (foundGamesSaves) {
                  resolve(foundGamesSaves);
                }).catch(function (e) {
                  reject(e);
                });
              });
            },

            /**
             * Calculate the game save settings based on the game for which this is a save and our current difficulty
             * Each game will have a set of its own settings rules which we use and apply using associated difficulty level
	           * to the gameSave, called on every update and create
             * @param {String} gameId The game ID used to get game object, for which this is a save
             * @param {String} diffcultyId The diffculty ID used to get difficulty object, and affect the settings
             * @return {Promise} A promise that will be resolved/rejected with the results of the request.
             */
            applyLevel: function(gameId, diffcultyId) {

              var game, diffculty;

              return new Q.promise(function(resolve, reject) {

                gameSave.api.models.game.findOne({ where: { id: gameId }}).then(function(foundGame){
                  game = foundGame;
                  return gameSave.api.models.diffculty.findOne({ where: { id: diffcultyId }});

                }).then(function(foundDiffculty){
                  diffculty = foundDiffculty;

                  //example settingRules from a game object
                  /* {
                      health: {
                        type:Number,
                        default:1
                        multiplier: 2,
                        min: 0,
                        max: 100
                        }
                      }
                  */

                  var setting;
			            Object.keys(game.settingsRules).map(function(key){
                    setting = game.settingsRules[key];
                    gameSave.settings[key] = Math.max(setting.max, (setting.default + (diffculty.level * setting.multiplier)));
                  });

                  resolve(gameSave);
                });
              })
            }
        }
    });

    return game;
};
