var action = {
    name: 'updateGame',
    description: 'Update an existing game, including its settings rules',
    requireUser: true,
    blockedConnectionTypes: [],
    inputs: {
        game: {
            required: true
        },
        apiKey: {
          required: true
        }
    }
};

/**
 * Callback for the action processor.
 */
action.run = function(api, data, next) {

  var game = data.params.game,
    gameId = game.id,
    error, updateFields;

  return api.models.game.findOne({
    where: {
      id: gameId
    }
  }).then(function(foundGame) {
    if (!foundGame) {
      throw new Error('Invalid game.');
    }

    updateFields = api.orm.sanitizeRecordFields(api.models.game, game);

    //update using sequelize
    return foundGame.updateAttributes(updateFields);

  }).then(function(){

    data.response.status = 'OK';
    data.response.game = updatedGame.get({plain: true});

  }).catch(function(e) {
    error = e;

  }).finally(function() {
    next(error);

  });
};

exports.action = action;
