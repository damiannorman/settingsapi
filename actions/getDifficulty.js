var action = {
    name: 'getDiffculty',
    description: 'Get a single difficulty by its ID.',
    blockedConnectionTypes: [],
    inputs: {
        difficultyId: {
            required: false,
            description: 'The ID of the setting to retrieve.'
        },
        category: {
          required: false,
          description: 'The category name of the assoicated category'
        },
        level: {
          required: false,
          description: 'Numeric level of difficulty'
        }
    }
};

/**
 * Callback for the action processor.
 */
action.run = function(api, data, next) {
    var error;

    return api.models.difficulty.getDeepRecord(data.params.difficultyId).then(function(difficulty) {
      data.response.status = 'OK';
      data.response.difficulty = difficulty.get({ plain: true });

    }).catch(function(e) {
      error = api.validations.processError(e);

    }).finally(function() {
      next(error);

    });
};

exports.action = action;
