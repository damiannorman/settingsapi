var action = {
    name: 'createGameSave',
    description: 'Create a game save',
    requireUser: true,
    inputs: {
        gameSave: {
            required: true
        }
    }
};

/**
 * Callback for the action processor.
 */
action.run = function(api, data, next) {

  var newGameSave, error;

  return api.orm.sanitizeRecordFields(api.models.gameSave, data.params).then(function(createdGameSave){

    newGameSave = createdGameSave;
    return newGameSave.applyLevel(data.params.gameSave.gameId, data.params.gameSave.difficultyId);

  }).then(function(gameSave){

    data.response.status = 'OK';
    data.response.gameSave = gameSave.get({ plain: true });

  }).catch(function(e) {
    error = e;

  }).finally(function() {
      next(error);

  });
};

exports.action = action;
