var action = {
    name: 'getDifficulties',
    description: 'Get a list of all of the difficulties',
    blockedConnectionTypes: [],
    inputs: {
      category: {
        required: false,
        description: 'The ID of the setting to retrieve.'
      },
      level: {
        required: false,
        description: 'Numeric level of difficulty'
      }
    }
};

/**
 * Callback for the action processor.
 */
action.run = function(api, data, next) {
    var error;

    api.models.difficulty.getDeepRecords(data.params.level, data.params.category).then(function(difficulties) {

      data.response.status = 'OK';
      data.response.difficulties = difficulties;

    }).catch(function(e) {
      error = api.validations.processError(e);

    }).finally(function() {
      next(error);

    });
};

exports.action = action;
