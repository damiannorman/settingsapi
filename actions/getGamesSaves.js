var action = {
    name: 'getGamesSaves',
    description: 'Get a list of all of the game saves, including its settings, optionally by current difficulty level or category',
    blockedConnectionTypes: [],
    inputs: {
        level: {
            required: false,
            description: 'The current level of difficulty on the game save'
        },
        category: {
            required: false,
            description: 'The current category of difficulty on the game save'
        }
    }
};

/**
 * Callback for the action processor.
 */
action.run = function(api, data, next) {
    var error;

    //api.models.game.getDeepRecord, to get difficulty
    api.models.gameSave.getDeepRecords(data.params.level, data.params.category).then(function(gamesSaves){
      data.response.status = 'OK';
      data.response.games = gamesSaves;

    }).catch(function(e) {
        error = api.validations.processError(e);

    }).finally(function() {
        next(error);

    });
};

exports.action = action;
