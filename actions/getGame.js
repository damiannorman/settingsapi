var action = {
    name: 'getGame',
    description: 'Get a single game by its ID.',
    blockedConnectionTypes: [],
    inputs: {
        gameId: {
            required: true,
            description: 'The ID of the game to retrieve.'
        }
    }
};

/**
 * Callback for the action processor.
 */
action.run = function(api, data, next) {
    var error;

  api.models.game.getDeepRecord(data.params.gameId).then(function(game){
    data.response.status = 'OK';
    data.response.games = game;

  }).catch(function(e) {
    error = api.validations.processError(e);

  }).finally(function() {
    next(error);

  });
};

exports.action = action;
